﻿namespace FormForNeuronSystem
{
    partial class NeuronForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.InputLayersTextBox = new System.Windows.Forms.TextBox();
            this.HidenLayersTextBox = new System.Windows.Forms.TextBox();
            this.OutputLayersTextBox = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.CreateTopologyButton = new System.Windows.Forms.Button();
            this.LearningRateTextBox = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.LearnNEpoch = new System.Windows.Forms.Button();
            this.CountOfEpochTextBox = new System.Windows.Forms.TextBox();
            this.LeatnOneEpoch = new System.Windows.Forms.Button();
            this.LoadDataSetButton = new System.Windows.Forms.Button();
            this.UseNeuronSystemButton = new System.Windows.Forms.Button();
            this.ResetButton = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(209, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Количество входных нейронов";
            // 
            // InputLayersTextBox
            // 
            this.InputLayersTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.InputLayersTextBox.Location = new System.Drawing.Point(12, 28);
            this.InputLayersTextBox.Name = "InputLayersTextBox";
            this.InputLayersTextBox.Size = new System.Drawing.Size(206, 22);
            this.InputLayersTextBox.TabIndex = 1;
            this.InputLayersTextBox.Text = "25";
            this.InputLayersTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // HidenLayersTextBox
            // 
            this.HidenLayersTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.HidenLayersTextBox.Location = new System.Drawing.Point(12, 75);
            this.HidenLayersTextBox.Name = "HidenLayersTextBox";
            this.HidenLayersTextBox.Size = new System.Drawing.Size(206, 22);
            this.HidenLayersTextBox.TabIndex = 3;
            this.HidenLayersTextBox.Text = "10";
            this.HidenLayersTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // OutputLayersTextBox
            // 
            this.OutputLayersTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.OutputLayersTextBox.Location = new System.Drawing.Point(12, 120);
            this.OutputLayersTextBox.Name = "OutputLayersTextBox";
            this.OutputLayersTextBox.Size = new System.Drawing.Size(206, 22);
            this.OutputLayersTextBox.TabIndex = 5;
            this.OutputLayersTextBox.Text = "1";
            this.OutputLayersTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(9, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(218, 16);
            this.label3.TabIndex = 4;
            this.label3.Text = "Количество выходных нейронов";
            // 
            // CreateTopologyButton
            // 
            this.CreateTopologyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CreateTopologyButton.Location = new System.Drawing.Point(12, 192);
            this.CreateTopologyButton.Name = "CreateTopologyButton";
            this.CreateTopologyButton.Size = new System.Drawing.Size(206, 31);
            this.CreateTopologyButton.TabIndex = 6;
            this.CreateTopologyButton.Text = "Создать";
            this.CreateTopologyButton.UseVisualStyleBackColor = true;
            this.CreateTopologyButton.Click += new System.EventHandler(this.CreateTopologyButton_Click);
            // 
            // LearningRateTextBox
            // 
            this.LearningRateTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LearningRateTextBox.Location = new System.Drawing.Point(12, 164);
            this.LearningRateTextBox.Name = "LearningRateTextBox";
            this.LearningRateTextBox.Size = new System.Drawing.Size(206, 22);
            this.LearningRateTextBox.TabIndex = 10;
            this.LearningRateTextBox.Text = "0,1";
            this.LearningRateTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(9, 145);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(219, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = "Коэфициент скорости обучения";
            // 
            // LearnNEpoch
            // 
            this.LearnNEpoch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LearnNEpoch.Location = new System.Drawing.Point(284, 130);
            this.LearnNEpoch.Name = "LearnNEpoch";
            this.LearnNEpoch.Size = new System.Drawing.Size(231, 31);
            this.LearnNEpoch.TabIndex = 12;
            this.LearnNEpoch.Text = "Обучить N раз";
            this.LearnNEpoch.UseVisualStyleBackColor = true;
            this.LearnNEpoch.Click += new System.EventHandler(this.LearnNEpoch_Click);
            // 
            // CountOfEpochTextBox
            // 
            this.CountOfEpochTextBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.CountOfEpochTextBox.Location = new System.Drawing.Point(284, 167);
            this.CountOfEpochTextBox.Name = "CountOfEpochTextBox";
            this.CountOfEpochTextBox.Size = new System.Drawing.Size(231, 26);
            this.CountOfEpochTextBox.TabIndex = 11;
            this.CountOfEpochTextBox.Text = "2";
            this.CountOfEpochTextBox.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // LeatnOneEpoch
            // 
            this.LeatnOneEpoch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LeatnOneEpoch.Location = new System.Drawing.Point(284, 93);
            this.LeatnOneEpoch.Name = "LeatnOneEpoch";
            this.LeatnOneEpoch.Size = new System.Drawing.Size(231, 31);
            this.LeatnOneEpoch.TabIndex = 0;
            this.LeatnOneEpoch.Text = "Обучить 1 раз";
            this.LeatnOneEpoch.UseVisualStyleBackColor = true;
            this.LeatnOneEpoch.Click += new System.EventHandler(this.LearnOneEpoch_Click);
            // 
            // LoadDataSetButton
            // 
            this.LoadDataSetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LoadDataSetButton.Location = new System.Drawing.Point(284, 12);
            this.LoadDataSetButton.Name = "LoadDataSetButton";
            this.LoadDataSetButton.Size = new System.Drawing.Size(231, 31);
            this.LoadDataSetButton.TabIndex = 11;
            this.LoadDataSetButton.Text = "Загрузить обучающую выборку";
            this.LoadDataSetButton.UseVisualStyleBackColor = true;
            this.LoadDataSetButton.Click += new System.EventHandler(this.LoadDataSetButton_Click);
            // 
            // UseNeuronSystemButton
            // 
            this.UseNeuronSystemButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.UseNeuronSystemButton.Location = new System.Drawing.Point(284, 49);
            this.UseNeuronSystemButton.Name = "UseNeuronSystemButton";
            this.UseNeuronSystemButton.Size = new System.Drawing.Size(231, 31);
            this.UseNeuronSystemButton.TabIndex = 14;
            this.UseNeuronSystemButton.Text = "Определить изображение";
            this.UseNeuronSystemButton.UseVisualStyleBackColor = true;
            this.UseNeuronSystemButton.Click += new System.EventHandler(this.UseNeuronSystemButton_Click);
            // 
            // ResetButton
            // 
            this.ResetButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.ResetButton.Location = new System.Drawing.Point(12, 229);
            this.ResetButton.Name = "ResetButton";
            this.ResetButton.Size = new System.Drawing.Size(206, 31);
            this.ResetButton.TabIndex = 18;
            this.ResetButton.Text = "Сброс\r\n";
            this.ResetButton.UseVisualStyleBackColor = true;
            this.ResetButton.Click += new System.EventHandler(this.ResetButton_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(209, 16);
            this.label4.TabIndex = 19;
            this.label4.Text = "Количество скрытых нейронов";
            // 
            // NeuronForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(527, 268);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LearnNEpoch);
            this.Controls.Add(this.LearningRateTextBox);
            this.Controls.Add(this.CountOfEpochTextBox);
            this.Controls.Add(this.LeatnOneEpoch);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.InputLayersTextBox);
            this.Controls.Add(this.CreateTopologyButton);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.OutputLayersTextBox);
            this.Controls.Add(this.HidenLayersTextBox);
            this.Controls.Add(this.ResetButton);
            this.Controls.Add(this.LoadDataSetButton);
            this.Controls.Add(this.UseNeuronSystemButton);
            this.Name = "NeuronForm";
            this.Text = "MainWindow";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox InputLayersTextBox;
        private System.Windows.Forms.TextBox HidenLayersTextBox;
        private System.Windows.Forms.TextBox OutputLayersTextBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button CreateTopologyButton;
        private System.Windows.Forms.TextBox LearningRateTextBox;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox CountOfEpochTextBox;
        private System.Windows.Forms.Button LeatnOneEpoch;
        private System.Windows.Forms.Button LearnNEpoch;
        private System.Windows.Forms.Button LoadDataSetButton;
        private System.Windows.Forms.Button UseNeuronSystemButton;
        private System.Windows.Forms.Button ResetButton;
        private System.Windows.Forms.Label label4;
    }
}
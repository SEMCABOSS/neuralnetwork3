﻿namespace NeuralLibrary
{
    public class BiasNeuron : Neuron
    {
        public BiasNeuron() : base() { }

        /// <summary>
        /// Функция активация (сигмоида)
        /// </summary>
        /// <param name="x">Сумма</param>
        /// <returns>Значение после активации</returns>
        public override double ActivationFunction(double x)
        {
            return 1.0;
        }

        /// <summary>
        /// Произдвоная функция активации (сигмоида)
        /// </summary>
        /// <param name="x">Сумма</param>
        /// <returns>Значение после активации</returns>
        public override double ActivationFunctionDx(double x)
        {
            return 0;
        }
    }
}

﻿namespace NeuralLibrary
{
    public abstract class LearningStrategy
    {
        public NeuralNetwork Network { get; set; }

        public LearningStrategy(NeuralNetwork network)
        {
            Network = network;
        }

        /// <summary>
        /// Стратегия обучения
        /// </summary>
        /// <param name="expectedOutput">Ожидаемые выходные сигналы.</param>
        /// <param name="inputSignals">Входные сигналы.</param>
        /// <returns></returns>
        public abstract double Learn(double expectedOutput, params double[] inputSignals);
    }
}
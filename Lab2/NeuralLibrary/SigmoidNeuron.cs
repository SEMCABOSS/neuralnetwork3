﻿using System;

namespace NeuralLibrary
{
    public class SigmoidNeuron : Neuron
    {
        public SigmoidNeuron() : base() { }

        /// <summary>
        /// Функция активация (сигмоида)
        /// </summary>
        /// <param name="x">Сумма</param>
        /// <returns>Значение после активации</returns>
        public override double ActivationFunction(double x)
        {
            return 1.0 / (1.0 + Math.Exp(-x));
        }

        /// <summary>
        /// Произдвоная функция активации (сигмоида)
        /// </summary>
        /// <param name="x">Сумма</param>
        /// <returns>Значение после активации</returns>
        public override double ActivationFunctionDx(double x)
        {
            double sigmoid = ActivationFunction(x);
            return sigmoid / (1 - sigmoid);
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace NeuralLibrary.Tests
{
    [TestClass()]
    public class NeuronNetworkTests
    {
        [TestMethod()]
        public void BackPropagationTest()
        {
            List<DataSet> dataset = new List<DataSet>();
            //FillOnes(dataset);
            //FillZeros(dataset);
            FillFour(dataset);

            //var topology = new TopologyOfNetwork(25, 1, 0.1, 15, 10, 5);
            //Topology topology = new Topology(25, 1, 0.1, 15);
            Topology topology = new Topology(26, 1, 0.1, 15);

            NeuralNetwork neuralNetwork = new NeuralNetwork(topology);
            LearningStrategy strategy = new BackPropagationStrategy(neuralNetwork);
            neuralNetwork.learningStrategy = strategy;

            neuralNetwork.CreateLayers<SigmoidNeuron>();
            neuralNetwork.Layers[0].Neurons[25] = new BiasNeuron();
            neuralNetwork.Topology.InputCount = 25;

            //double difference = neuralNetwork.Learn(dataset, 10);
            var difference = neuralNetwork.Learn(dataset, 10000);

            List<double> results = new List<double>();
            foreach (var data in dataset)
            {
                var res = neuralNetwork.FeedForwardSignals(data.InputsSignals).Output;
                results.Add(res);
            }

            for (int i = 0; i < results.Count; i++)
            {
                double expected = Math.Round(dataset[i].Expected, 0);
                double actual = Math.Round(results[i], 0);
                Assert.AreEqual(expected, actual);
            }
        }

        [TestMethod()]
        public void WidrowHoffTest()
        {
            List<DataSet> dataset = new List<DataSet>();
            //FillOnes(dataset);
            //FillZeros(dataset);
            FillFour(dataset);

            //var topology = new TopologyOfNetwork(25, 1, 0.1, 15, 10, 5);
            Topology topology = new Topology(25, 1, 0.1, 5);

            NeuralNetwork neuralNetwork = new NeuralNetwork(topology);
            LearningStrategy strategy = new WidrowHoffStrategy(neuralNetwork);
            neuralNetwork.learningStrategy = strategy;

            neuralNetwork.CreateLayers<SigmoidNeuron>();
            double difference = neuralNetwork.Learn(dataset, 10000);
            //var difference = neuralNetwork.Learn(dataset, 10000);

            List<double> results = new List<double>();
            foreach (var data in dataset)
            {
                var res = neuralNetwork.FeedForwardSignals(data.InputsSignals).Output;
                results.Add(res);
            }

            for (int i = 0; i < results.Count; i++)
            {
                double expected = Math.Round(dataset[i].Expected, 0);
                double actual = Math.Round(results[i] - 0.1, 0);
                Assert.AreEqual(expected, actual);
            }
        }

        private void FillFour(List<DataSet> dataset)
        {
            dataset.Add(new DataSet(1, new double[]{
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0
            }));
            dataset.Add(new DataSet(1, new double[]{
                0, 0, 1, 0, 0,
                0, 1, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 1, 1, 1, 0
            }));

            dataset.Add(new DataSet(0, new double[]{
                0, 0, 1, 0, 0,
                0, 1, 0, 1, 0,
                0, 1, 0, 1, 0,
                0, 1, 0, 1, 0,
                0, 0, 1, 0, 0
            }));
            dataset.Add(new DataSet(0, new double[]{
                0, 1, 1, 0, 0,
                1, 0, 0, 1, 0,
                1, 0, 0, 1, 0,
                1, 0, 0, 1, 0,
                0, 1, 1, 0, 0
            }));
        }

        private void FillOnes(List<DataSet> dataset)
        {
            dataset.Add(new DataSet(1, new double[]{
                1, 0, 0, 0, 0,
                1, 0, 0, 0, 0,
                1, 0, 0, 0, 0,
                1, 0, 0, 0, 0,
                1, 0, 0, 0, 0
            }));
            dataset.Add(new DataSet(1, new double[]{
                0, 1, 0, 0, 0,
                0, 1, 0, 0, 0,
                0, 1, 0, 0, 0,
                0, 1, 0, 0, 0,
                0, 1, 0, 0, 0
            }));
            dataset.Add(new DataSet(1, new double[]{
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0
            }));
            dataset.Add(new DataSet(1, new double[]{
                0, 0, 0, 1, 0,
                0, 0, 0, 1, 0,
                0, 0, 0, 1, 0,
                0, 0, 0, 1, 0,
                0, 0, 0, 1, 0
            }));
            dataset.Add(new DataSet(1, new double[]{
                0, 0, 0, 0, 1,
                0, 0, 0, 0, 1,
                0, 0, 0, 0, 1,
                0, 0, 0, 0, 1,
                0, 0, 0, 0, 1
            }));

            dataset.Add(new DataSet(1, new double[]{
                0, 1, 0, 0, 0,
                1, 1, 0, 0, 0,
                0, 1, 0, 0, 0,
                0, 1, 0, 0, 0,
                0, 1, 0, 0, 0
            }));
            dataset.Add(new DataSet(1, new double[]{
                0, 0, 1, 0, 0,
                0, 1, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0
            }));
            dataset.Add(new DataSet(1, new double[]{
                0, 0, 0, 1, 0,
                0, 0, 1, 1, 0,
                0, 0, 0, 1, 0,
                0, 0, 0, 1, 0,
                0, 0, 0, 1, 0
            }));
            dataset.Add(new DataSet(1, new double[]{
                0, 0, 0, 0, 1,
                0, 0, 0, 1, 1,
                0, 0, 0, 0, 1,
                0, 0, 0, 0, 1,
                0, 0, 0, 0, 1
            }));

            dataset.Add(new DataSet(1, new double[]{
                0, 1, 0, 0, 0,
                1, 1, 0, 0, 0,
                0, 1, 0, 0, 0,
                0, 1, 0, 0, 0,
                1, 1, 1, 0, 0
            }));
            dataset.Add(new DataSet(1, new double[]{
                0, 0, 1, 0, 0,
                0, 1, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 0, 1, 0, 0,
                0, 1, 1, 1, 0
            }));
            dataset.Add(new DataSet(1, new double[]{
                0, 0, 0, 1, 0,
                0, 0, 1, 1, 0,
                0, 0, 0, 1, 0,
                0, 0, 0, 1, 0,
                0, 0, 1, 1, 1
            }));
        }

        private void FillZeros(List<DataSet> dataset)
        {
            dataset.Add(new DataSet(0, new double[]{
                0, 1, 0, 0, 0,
                1, 0, 1, 0, 0,
                1, 0, 1, 0, 0,
                1, 0, 1, 0, 0,
                0, 1, 0, 0, 0
            }));
            dataset.Add(new DataSet(0, new double[]{
                0, 0, 1, 0, 0,
                0, 1, 0, 1, 0,
                0, 1, 0, 1, 0,
                0, 1, 0, 1, 0,
                0, 0, 1, 0, 0
            }));
            dataset.Add(new DataSet(0, new double[]{
                0, 0, 0, 1, 0,
                0, 0, 1, 0, 1,
                0, 0, 1, 0, 1,
                0, 0, 1, 0, 1,
                0, 0, 0, 1, 0
            }));

            dataset.Add(new DataSet(0, new double[]{
                0, 1, 1, 0, 0,
                1, 0, 0, 1, 0,
                1, 0, 0, 1, 0,
                1, 0, 0, 1, 0,
                0, 1, 1, 0, 0
            }));
            dataset.Add(new DataSet(0, new double[]{
                0, 0, 1, 1, 0,
                0, 1, 0, 0, 1,
                0, 1, 0, 0, 1,
                0, 1, 0, 0, 1,
                0, 0, 1, 1, 0
            }));

            dataset.Add(new DataSet(0, new double[]{
                0, 1, 1, 1, 0,
                1, 0, 0, 0, 1,
                1, 0, 0, 0, 1,
                1, 0, 0, 0, 1,
                0, 1, 1, 1, 0
            }));

            dataset.Add(new DataSet(0, new double[]{
                1, 1, 1, 0, 0,
                1, 0, 1, 0, 0,
                1, 0, 1, 0, 0,
                1, 0, 1, 0, 0,
                1, 1, 1, 0, 0
            }));
            dataset.Add(new DataSet(0, new double[]{
                0, 1, 1, 1, 0,
                0, 1, 0, 1, 0,
                0, 1, 0, 1, 0,
                0, 1, 0, 1, 0,
                0, 1, 1, 1, 0
            }));
            dataset.Add(new DataSet(0, new double[]{
                0, 0, 1, 1, 1,
                0, 0, 1, 0, 1,
                0, 0, 1, 0, 1,
                0, 0, 1, 0, 1,
                0, 0, 1, 1, 1
            }));

            dataset.Add(new DataSet(0, new double[]{
                1, 1, 1, 1, 0,
                1, 0, 0, 1, 0,
                1, 0, 0, 1, 0,
                1, 0, 0, 1, 0,
                1, 1, 1, 1, 0
            }));
            dataset.Add(new DataSet(0, new double[]{
                0, 1, 1, 1, 1,
                0, 1, 0, 0, 1,
                0, 1, 0, 0, 1,
                0, 1, 0, 0, 1,
                0, 1, 1, 1, 1
            }));

            dataset.Add(new DataSet(0, new double[]{
                1, 1, 1, 1, 1,
                1, 0, 0, 0, 1,
                1, 0, 0, 0, 1,
                1, 0, 0, 0, 1,
                1, 1, 1, 1, 1
            }));
        }
    }
}
﻿using NeuralLibrary;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RBNForm
{
    public partial class Form1 : Form
    {
        private List<DataSet> dataset = new List<DataSet>();
        private double[][] input;
        private double[][] output;

        public Form1()
        {
            InitializeComponent();
        }

        private void FunctionBtn_Click(object sender, EventArgs e)
        {
            int startX = -20;
            int endX = 20;
            double step = 2;
            double normal = 19000;
            int inputCount = (int)((endX - startX) / step);

            input = new double[inputCount][];
            output = new double[inputCount][];

            int n = 0;
            for (double x = startX; x < endX; x += step)
            {
                //double y = x * x * x * Math.Exp(Math.Cos(x)) / normal;
                double y = Math.Sin(x);
                input[n] = new double[] { x };
                output[n] = new double[] { y };
                dataset.Add(new DataSet(y, new double[] { x }));
                n++;

                Graphic.Series["function"].Points.AddXY(x, y);
            }
        }

        private void PerceptronBtn_Click(object sender, EventArgs e)
        {
            Topology topology = new Topology(2, 1, 0.1, 5);

            NeuralNetwork neuralNetwork = new NeuralNetwork(topology);
            LearningStrategy strategy = new BackPropagationStrategy(neuralNetwork);
            neuralNetwork.learningStrategy = strategy;

            neuralNetwork.CreateLayers<SigmoidNeuron>();
            neuralNetwork.Layers[0].Neurons[1] = new BiasNeuron();
            neuralNetwork.Topology.InputCount = 1;


            double error, eps = 0.001;
            int epoch = 0, maxEpoch = 100000;
            do
            {
                error = 0;
                for (int j = 0; j < input.Length; j++)
                {
                    error = neuralNetwork.Learn(dataset, 1);
                }

                ErrorsGraph.Series["Perceptron"].Points
                    .AddXY(epoch, error);
                break;
            }
            while (error > eps && epoch++ < maxEpoch);

            foreach (var data in dataset)
            {
                Graphic.Series["Perceptron"].Points
                    .AddXY(data.InputsSignals[0], neuralNetwork.FeedForwardSignals(data.InputsSignals).Output);
            }
        }

        private void RBNBtn_Click(object sender, EventArgs e)
        {
        }
    }
}

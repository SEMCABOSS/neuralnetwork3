﻿namespace NeuralLibrary
{
    public class DataSet
    {
        public double Expected { get; private set; }
        public double[] InputsSignals { get; private set; }

        public DataSet(double expected, double[] inputsSignals)
        {
            Expected = expected;
            InputsSignals = inputsSignals;
        }
    }
}

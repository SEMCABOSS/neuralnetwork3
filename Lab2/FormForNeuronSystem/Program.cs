﻿using NeuralLibrary;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace FormForNeuronSystem
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new NeuronForm());
            }
            else
            {
                string datasetPath = args[0];
                string outputPath = args[1];

                List<DataSet> dataSets = new List<DataSet>();
                List<string> paths = FillDataset(dataSets, datasetPath);

                Topology topology = new Topology(25, 1, 0.1, 15);

                NeuralNetwork backPropNetwork = new NeuralNetwork(topology);
                backPropNetwork.learningStrategy = new BackPropagationStrategy(backPropNetwork);
                backPropNetwork.CreateLayers<SigmoidNeuron>();

                NeuralNetwork widrowHoffNetwork = new NeuralNetwork(topology);
                widrowHoffNetwork.learningStrategy = new WidrowHoffStrategy(widrowHoffNetwork);
                widrowHoffNetwork.CreateLayers<SigmoidNeuron>();

                backPropNetwork.Learn(dataSets, 1000);
                widrowHoffNetwork.Learn(dataSets, 1000);

                using (StreamWriter writer = new StreamWriter(outputPath))
                {
                    for (int i = 0; i < dataSets.Count; i++)
                    {
                        double backPropRes = backPropNetwork.FeedForwardSignals(dataSets[i].InputsSignals).Output;
                        double widrowHoffRes = widrowHoffNetwork.FeedForwardSignals(dataSets[i].InputsSignals).Output - 0.1;

                        WriteResult(writer, paths[i], backPropRes, widrowHoffRes);
                        WriteResult(Console.Out, paths[i], backPropRes, widrowHoffRes);
                    }
                }
            }
        }

        private static List<string> FillDataset(List<DataSet> dataSet, string datasetPath)
        {
            List<string> paths = new List<string>();
            using (StreamReader reader = new StreamReader(datasetPath))
            {
                while (!reader.EndOfStream)
                {
                    string line = reader.ReadLine();
                    double expected = double.Parse(line.Substring(0, 1));
                    string path = line.Substring(2);
                    using (Bitmap bitmap = new Bitmap(path))
                    {
                        dataSet.Add(bitmap.ToDataSet(expected));
                        paths.Add(path);
                    }
                }
            }
            return paths;
        }

        private static void WriteResult(TextWriter writer, string path, double backPropRes, double widrowHoffRes)
        {
            writer.WriteLine(path + ':');
            writer.WriteLine("BackProp: " + backPropRes);
            writer.WriteLine("WidrowHoff: " + widrowHoffRes);
        }
    }
}

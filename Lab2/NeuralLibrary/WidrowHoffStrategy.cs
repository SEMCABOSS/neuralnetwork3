﻿namespace NeuralLibrary
{
    public class WidrowHoffStrategy : LearningStrategy
    {
        public WidrowHoffStrategy(NeuralNetwork network)
            : base(network)
        {
        }

        /// <summary>
        /// Метод обучения Уидроу-Хоффа.
        /// </summary>
        /// <param name="expectedOutput">Ожидаемое значение</param>
        /// <param name="inputSignals">Входные сигналы</param>
        /// <returns></returns>
        /// <summary>
        public override double Learn(double expectedOutput, params double[] inputSignals)
        {
            double actualOutput = Network.FeedForwardSignals(inputSignals).Output;

            double difference = actualOutput - expectedOutput;

            for (int j = Network.Layers.Count - 1; j >= 0; j--)
            {
                Layer currentLayer = Network.Layers[j];

                for (int i = 0; i < currentLayer.Count; i++)
                {
                    Neuron neuron = currentLayer.Neurons[i];
                    double error = expectedOutput - neuron.Output;
                    UpdateWeights(neuron, error);
                }
            }

            return difference * difference / 2;
        }

        /// <summary>
        /// Изменение весов нейрона.
        /// </summary>
        /// <param name="neuron">Нейрон, веса которого будут изменяться.</param>
        /// <param name="error">Значение ошибки.</param>
        private void UpdateWeights(Neuron neuron, double error)
        {
            if (neuron.NeuronType == TypeOfNeuron.Input) return;

            neuron.Delta = error * neuron.ActivationFunctionDx(neuron.Output);

            for (int i = 0; i < neuron.Weights.Count; i++)
            {
                var weight = neuron.Weights[i];
                var input = neuron.Inputs[i];

                var newWeigth = weight + input * neuron.Delta * Network.Topology.LearningRate;
                neuron.Weights[i] = newWeigth;
            }
        }
    }
}

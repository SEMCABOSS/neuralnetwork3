﻿namespace SVMLib
{
    public interface ISupportVectorMachineLearning
    {
        double[] Run();
    }
}
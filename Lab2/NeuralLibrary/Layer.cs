﻿using System;
using System.Collections.Generic;

namespace NeuralLibrary
{
    /// <summary>
    /// Класс слоя нейроной сети
    /// </summary>
    public class Layer
    {
        /// <summary>
        /// Нейроны на данном слое
        /// </summary>
        public List<Neuron> Neurons { get; private set; }
        /// <summary>
        /// Количество нейроннов на данном слое
        /// </summary>
        public int Count => Neurons?.Count ?? 0;
        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="neurons">Нейроны на данном слое</param>
        /// <param name="type">Тип нейронов на данном слое</param>
        public Layer(List<Neuron> neurons, TypeOfNeuron type = TypeOfNeuron.Intermediate)
        {
            if (neurons == null)
                throw new Exception("List not consists neuron.");

            foreach (Neuron neuron in neurons)
                if (neuron.NeuronType != type)
                    throw new Exception("Wrong type of neuron in list.");

            Neurons = neurons;
        }
        /// <summary>
        /// Возращает выходные сигналы всех нейронов на данном слое
        /// </summary>
        /// <returns>Список выходных сигналов</returns>
        public List<double> GetSignals()
        {
            List<double> results = new List<double>();

            foreach (Neuron neuron in Neurons)
                results.Add(neuron.Output);

            return results;
        }
    }
}

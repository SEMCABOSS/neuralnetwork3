﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace FormForNeuronSystem
{
    public partial class NeuronGraphic : Form
    {
        public GraphicsData widrowHoffData { get; set; }
        public GraphicsData backPropData { get; set; }

        public NeuronGraphic()
        {
            InitializeComponent();
            widrowHoffData = new GraphicsData();
            backPropData = new GraphicsData();
        }

        public void DrawFucntion()
        {
            var data = backPropData.Data.Last();

            Graphic.Series["Back Propagation"].Points.AddXY(data.Key, data.Value);

            data = widrowHoffData.Data.Last();

            Graphic.Series["Widrow-Hoff"].Points.AddXY(data.Key, data.Value);
        }
    }

    public class GraphicsData
    {
        public Dictionary<int, double> Data { get; private set; }

        public GraphicsData()
        {
            Data = new Dictionary<int, double>();
        }

        public void Add(int epoch, double dif)
        {
            Data.Add(epoch, dif);
        }
    }
}

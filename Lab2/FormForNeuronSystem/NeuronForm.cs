﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using NeuralLibrary;

namespace FormForNeuronSystem
{
    public partial class NeuronForm : Form
    {
        private bool isDataSetLoaded = false;
        private bool isTopologyCreate = false;

        private Topology topology;
        private NeuralNetwork backPropNetwork;
        private NeuralNetwork widrowHoffNetwork;
        private NeuronGraphic graphicsForm;
        private List<DataSet> dataSets;

        public NeuronForm()
        {
            InitializeComponent();
            dataSets = new List<DataSet>();
        }

        private void CreateTopologyButton_Click(object sender, EventArgs e)
        {
            if (isTopologyCreate) return;

            int input, output;
            double learningRate;
            string[] hiddensText;
            int[] hidden;

            if (!int.TryParse(InputLayersTextBox.Text, out input) ||
                !int.TryParse(OutputLayersTextBox.Text, out output) ||
                !double.TryParse(LearningRateTextBox.Text, out learningRate))
            {
                MessageBox.Show("Неверные значения полей.");
                return;
            }

            hiddensText = HidenLayersTextBox.Text.Split(new char[] { ',' });
            hidden = new int[hiddensText.Length];
            for (int i = 0; i < hiddensText.Length; i++)
            {
                if (!int.TryParse(hiddensText[i], out hidden[i]))
                {
                    MessageBox.Show("Неверные значения полей.");
                    return;
                }
            }

            topology = new Topology(input, output, learningRate, hidden);
            backPropNetwork = new NeuralNetwork(topology);
            backPropNetwork.CreateLayers<SigmoidNeuron>();
            backPropNetwork.learningStrategy = new BackPropagationStrategy(backPropNetwork);

            widrowHoffNetwork = new NeuralNetwork(topology);
            widrowHoffNetwork.CreateLayers<SigmoidNeuron>();
            widrowHoffNetwork.learningStrategy = new WidrowHoffStrategy(widrowHoffNetwork);

            graphicsForm = new NeuronGraphic();
            graphicsForm.Show();

            isTopologyCreate = true;
        }

        private void LearnOneEpoch_Click(object sender, EventArgs e)
        {
            LearnNeuronNetwork(1);
        }

        private void LearnNEpoch_Click(object sender, EventArgs e)
        {
            int epoch;

            if(!int.TryParse(CountOfEpochTextBox.Text,out epoch))
            {
                MessageBox.Show("Неверные значения полей.");
                return;
            }

            LearnNeuronNetwork(epoch);
        }

        private void LearnNeuronNetwork(int epoch)
        {
            if(!isDataSetLoaded)
            {
                MessageBox.Show("Обучающая выборка не загружена.");
                return;
            }

            if (!isTopologyCreate)
            {
                MessageBox.Show("Нейронные сети не созданы.");
                return;
            }

            double difference = backPropNetwork.Learn(dataSets, epoch);
            graphicsForm.backPropData.Add(backPropNetwork.CountOfEpoch, difference);

            difference = widrowHoffNetwork.Learn(dataSets, epoch);
            graphicsForm.widrowHoffData.Add(widrowHoffNetwork.CountOfEpoch, difference);

            graphicsForm.DrawFucntion();
        }

        private void LoadDataSetButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                string file = openFile.FileName;
                using (StreamReader reader = new StreamReader(file))
                {
                    while (!reader.EndOfStream)
                    {
                        string line = reader.ReadLine();
                        double expected = double.Parse(line.Substring(0, 1));
                        string path = line.Substring(2);
                        using (Bitmap bitmap = new Bitmap(path))
                        {
                            dataSets.Add(bitmap.ToDataSet(expected));
                        }
                    }
                }

                isDataSetLoaded = true;
            }
        }   

        private void ResetButton_Click(object sender, EventArgs e)
        {
            isTopologyCreate = false;
            isDataSetLoaded = false;
            dataSets = new List<DataSet>();
            graphicsForm.Close();
        }

        private void UseNeuronSystemButton_Click(object sender, EventArgs e)
        {
            if (!isTopologyCreate)
            {
                MessageBox.Show("Нейронные сети не созданы.");
                return;
            }

            OpenFileDialog openFile = new OpenFileDialog();
            openFile.RestoreDirectory = true;

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                using (Bitmap bitmap = new Bitmap(openFile.FileName))
                {
                    DataSet dataset = bitmap.ToDataSet(0);
                    double backPropRes = backPropNetwork.FeedForwardSignals(dataset.InputsSignals).Output;
                    double widrowHoffRes = widrowHoffNetwork.FeedForwardSignals(dataset.InputsSignals).Output - 0.1;

                    MessageBox.Show("Widrow-Hoff: " + widrowHoffRes.ToString()
                        + " = " + Math.Round(widrowHoffRes).ToString()
                        + Environment.NewLine
                        + "Back Propagation: " + backPropRes.ToString()
                        + " = " + Math.Round(backPropRes).ToString());
                }
            }
        }
    }
}

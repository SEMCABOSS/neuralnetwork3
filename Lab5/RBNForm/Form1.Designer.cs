﻿namespace RBNForm
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea5 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend5 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea6 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend6 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.ErrorsGraph = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.RBNBtn = new System.Windows.Forms.Button();
            this.PerceptronBtn = new System.Windows.Forms.Button();
            this.FunctionBtn = new System.Windows.Forms.Button();
            this.Graphic = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.ErrorsGraph)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Graphic)).BeginInit();
            this.SuspendLayout();
            // 
            // ErrorsGraph
            // 
            chartArea5.Name = "ChartArea1";
            this.ErrorsGraph.ChartAreas.Add(chartArea5);
            legend5.Name = "Legend1";
            this.ErrorsGraph.Legends.Add(legend5);
            this.ErrorsGraph.Location = new System.Drawing.Point(769, 12);
            this.ErrorsGraph.Name = "ErrorsGraph";
            this.ErrorsGraph.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series11.BorderWidth = 2;
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series11.Color = System.Drawing.Color.MediumTurquoise;
            series11.Legend = "Legend1";
            series11.Name = "Perceptron";
            series11.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series11.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series12.BorderWidth = 2;
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series12.Legend = "Legend1";
            series12.Name = "RBN";
            this.ErrorsGraph.Series.Add(series11);
            this.ErrorsGraph.Series.Add(series12);
            this.ErrorsGraph.Size = new System.Drawing.Size(730, 608);
            this.ErrorsGraph.TabIndex = 10;
            this.ErrorsGraph.Text = "graphic";
            // 
            // RBNBtn
            // 
            this.RBNBtn.Location = new System.Drawing.Point(603, 626);
            this.RBNBtn.Name = "RBNBtn";
            this.RBNBtn.Size = new System.Drawing.Size(139, 22);
            this.RBNBtn.TabIndex = 9;
            this.RBNBtn.Text = "RBN";
            this.RBNBtn.UseVisualStyleBackColor = true;
            this.RBNBtn.Click += new System.EventHandler(this.RBNBtn_Click);
            // 
            // PerceptronBtn
            // 
            this.PerceptronBtn.Location = new System.Drawing.Point(298, 626);
            this.PerceptronBtn.Name = "PerceptronBtn";
            this.PerceptronBtn.Size = new System.Drawing.Size(139, 22);
            this.PerceptronBtn.TabIndex = 8;
            this.PerceptronBtn.Text = "Перцептрон";
            this.PerceptronBtn.UseVisualStyleBackColor = true;
            this.PerceptronBtn.Click += new System.EventHandler(this.PerceptronBtn_Click);
            // 
            // FunctionBtn
            // 
            this.FunctionBtn.Location = new System.Drawing.Point(12, 626);
            this.FunctionBtn.Name = "FunctionBtn";
            this.FunctionBtn.Size = new System.Drawing.Size(139, 22);
            this.FunctionBtn.TabIndex = 7;
            this.FunctionBtn.Text = "Функция";
            this.FunctionBtn.UseVisualStyleBackColor = true;
            this.FunctionBtn.Click += new System.EventHandler(this.FunctionBtn_Click);
            // 
            // Graphic
            // 
            chartArea6.Name = "ChartArea1";
            this.Graphic.ChartAreas.Add(chartArea6);
            legend6.Name = "Legend1";
            this.Graphic.Legends.Add(legend6);
            this.Graphic.Location = new System.Drawing.Point(12, 12);
            this.Graphic.Name = "Graphic";
            this.Graphic.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series13.BorderWidth = 2;
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series13.Color = System.Drawing.Color.MediumTurquoise;
            series13.Legend = "Legend1";
            series13.Name = "Perceptron";
            series13.XValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Int32;
            series13.YValueType = System.Windows.Forms.DataVisualization.Charting.ChartValueType.Double;
            series14.BorderWidth = 2;
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series14.Legend = "Legend1";
            series14.Name = "RBN";
            series15.BorderWidth = 2;
            series15.ChartArea = "ChartArea1";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series15.Color = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(192)))));
            series15.Legend = "Legend1";
            series15.Name = "Function";
            this.Graphic.Series.Add(series13);
            this.Graphic.Series.Add(series14);
            this.Graphic.Series.Add(series15);
            this.Graphic.Size = new System.Drawing.Size(730, 608);
            this.Graphic.TabIndex = 6;
            this.Graphic.Text = "graphic";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1511, 682);
            this.Controls.Add(this.ErrorsGraph);
            this.Controls.Add(this.RBNBtn);
            this.Controls.Add(this.PerceptronBtn);
            this.Controls.Add(this.FunctionBtn);
            this.Controls.Add(this.Graphic);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.ErrorsGraph)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Graphic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataVisualization.Charting.Chart ErrorsGraph;
        private System.Windows.Forms.Button RBNBtn;
        private System.Windows.Forms.Button PerceptronBtn;
        private System.Windows.Forms.Button FunctionBtn;
        private System.Windows.Forms.DataVisualization.Charting.Chart Graphic;
    }
}

